package com.example.chatapp;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.chatapp.Models.User;
import com.example.chatapp.databinding.ActivitySettingsBinding;
import com.example.chatapp.util.image.ImageSelectorUtils;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

public class SettingsActivity extends AppCompatActivity {

    ActivitySettingsBinding binding;

    FirebaseAuth mAuth;
    FirebaseDatabase database;
    FirebaseStorage storage;

    ImageView profilePicHolder, addNewImageBtn;
    EditText usernameHolder;
    Button settingsProfileCancelBtn, settingsProfileSaveBtn;
    ConstraintLayout settingsProfileCard;

    String username, orgUsername, pictureUri, orgPictureUri;

    ProgressDialog progressDialog;

    ConstraintLayout constraintLayout;
    ConstraintSet closedConstraintSet, openedConstraintSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        binding = ActivitySettingsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        getSupportActionBar().hide();

        progressDialog = new ProgressDialog(SettingsActivity.this);
        progressDialog.setTitle("Updating data");
        progressDialog.setMessage("Please wait \nValidation in progress..");

        database = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        storage = FirebaseStorage.getInstance();

        profilePicHolder = binding.settingsProfilePic;
        addNewImageBtn = binding.settingProfileAddNewImageBtn;
        usernameHolder = binding.settingsUserName;
        settingsProfileCancelBtn = binding.settingsProfileCancelBtn;
        settingsProfileSaveBtn = binding.settingsProfileSaveBtn;
        settingsProfileCard = binding.settingProfileCard;

        // VISUAL EFFECTS
        constraintLayout = binding.settingProfileCard;
        closedConstraintSet = new ConstraintSet();
        openedConstraintSet = new ConstraintSet();
        closedConstraintSet.clone(constraintLayout);
        openedConstraintSet.clone(constraintLayout);
        applyConstraintsInstructions(openedConstraintSet);

        // OPENING PROFILE SETTINGS - USER'S CARD CLICK
        binding.settingProfileCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCardView();
                checkSaveButton();
            }
        });

        // CANCEL BTN ACTION
        binding.settingsProfileCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeCardView();
                setUsersData();
            }
        });

        // BACK ARROW ACTION
        binding.settingsToolbarBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        // ADD IMAGE BTN ACTION
        binding.settingProfileAddNewImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int PERMISSION_ALL = 1;

                String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
                String[] missingPremisons = hasPermissions(permissions);

                if (missingPremisons.length != 0) {
                    ActivityCompat.requestPermissions(SettingsActivity.this, missingPremisons, PERMISSION_ALL);
                }
                someActivityResultLauncher.launch(selectImage());
            }
        });

        // SAVE BTN ACTION
        binding.settingsProfileSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = usernameHolder.getText().toString();

                if ((orgUsername.equals(username) || username.isEmpty())
                        && orgPictureUri.equals(pictureUri)) {
                    Toast.makeText(SettingsActivity.this, R.string.error_credentials_not_changed, Toast.LENGTH_SHORT).show();
                    return;
                }
                progressDialog.show();
                database.getReference().child("Users").child(Objects.requireNonNull(mAuth.getCurrentUser()).getUid())
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                User user = snapshot.getValue(User.class);

                                assert user != null;

                                user.setUserName(username);

                                if(!orgPictureUri.equals(pictureUri)){
                                    final StorageReference reference = storage.getReference()
                                            .child("profile_pic")
                                            .child(Objects.requireNonNull(FirebaseAuth.getInstance().getUid()));

                                    reference.putFile(Uri.fromFile(new File(pictureUri))).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                            reference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                @Override
                                                public void onSuccess(Uri uri) {

                                                    user.setProfilePic(uri.toString());

                                                    database.getReference()
                                                            .child("Users")
                                                            .child(Objects.requireNonNull(mAuth.getCurrentUser()).getUid())
                                                            .setValue(user)
                                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                @Override
                                                                public void onSuccess(Void unused) {
                                                                    setUsersData();

                                                                    orgPictureUri = pictureUri;
                                                                    orgUsername = username;

                                                                    progressDialog.dismiss();
                                                                    setUsersData();
                                                                    closeCardView();
                                                                }
                                                            });
                                                }
                                            });
                                        }
                                    });
                                }else{
                                    database.getReference()
                                            .child("Users")
                                            .child(Objects.requireNonNull(mAuth.getCurrentUser()).getUid())
                                            .setValue(user)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void unused) {
                                                    setUsersData();

                                                    orgPictureUri = pictureUri;
                                                    orgUsername = username;

                                                    progressDialog.dismiss();
                                                    setUsersData();
                                                    closeCardView();
                                                }
                                            });
                                }



                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {
                                Toast.makeText(SettingsActivity.this, R.string.error_user_not_found, Toast.LENGTH_LONG).show();
                            }
                        });
            }
        });

        // USERNAME TEXT EDIT WATCHER
        binding.settingsUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                username = usernameHolder.getText().toString();
                checkSaveButton();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        setUsersData();
    }


    // IMAGE SELECT UTILS
    public Intent selectImage() {
        return getPickIntent();
    }

    private Intent getPickIntent() {
        final List<Intent> intents = new ArrayList<Intent>();

        intents.add(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI));
        intents.add(setCameraIntents());

        if (intents.isEmpty()) return null;
        Intent result = Intent.createChooser(intents.remove(0), null);
        if (!intents.isEmpty()) {
            result.putExtra(Intent.EXTRA_INITIAL_INTENTS, intents.toArray(new Parcelable[]{}));
        }

        return result;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        //File storageDir = new File(String.valueOf(MediaStore.Images.Media.EXTERNAL_CONTENT_URI));
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        pictureUri = image.getAbsolutePath();
        return image;
    }

    private Intent setCameraIntents() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.chatapp",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            }
        }
        return takePictureIntent;
    }

    ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {

                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // There are no request codes
                        Intent data = result.getData();
                        String path = null;


                        if (data != null) {
                            File file = getBitmapFile(data);
                            pictureUri = file.getAbsolutePath();
                        }

                        ImageSelectorUtils.setImage(pictureUri, binding.settingsProfilePic);
                        checkSaveButton();
                    }
                }
            });

    public File getBitmapFile(Intent data) {
        Uri selectedImage = data.getData();
        Cursor cursor = getContentResolver().query(selectedImage, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
        cursor.moveToFirst();

        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        String selectedImagePath = cursor.getString(idx);
        cursor.close();

        return new File(selectedImagePath);
    }

    // PREMISSIONS UTILS
    public String[] hasPermissions(String[] premissions) {
        HashSet<String> missingPremisons = new HashSet<>();
        for (String premission : premissions) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(premission) == PackageManager.PERMISSION_GRANTED) {
                    Log.v("TAG", "Permission is granted");
                } else {
                    Log.v("TAG", "Permission is revoked");
                    missingPremisons.add(premission);
                }
            } else { //permission is automatically granted on sdk<23 upon installation
                Log.v("TAG", "Permission is granted");
            }
        }

        String[] y = new String[missingPremisons.size()];
        int c = 0;
        for (String x : missingPremisons) y[c++] = x;


        return y;
    }

    public boolean isStoragePermissionGranted(String premission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(premission)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {
                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{premission}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    // OTHER
    private void setUsersData() {
        database.getReference().child("Users").child(Objects.requireNonNull(mAuth.getCurrentUser()).getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        User user = snapshot.getValue(User.class);

                        if (user != null) {
                            username = user.getUserName();
                            orgUsername = username;
                            pictureUri = user.getProfilePic() == null? "": user.getProfilePic();
                            orgPictureUri = pictureUri == null ? "" : pictureUri;

                            usernameHolder.setText(user.getUserName());
                            Picasso.get()
                                    .load(user.getProfilePic())
                                    .placeholder(R.drawable.avatar)
                                    .into(profilePicHolder);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void checkSaveButton() {
        if (!orgUsername.equals(username) && !username.isEmpty()
                || !orgPictureUri.equals(pictureUri)) {
            settingsProfileSaveBtn.setEnabled(true);
            settingsProfileSaveBtn.setTextColor(getResources().getColor(R.color.lightText));
        } else {
            settingsProfileSaveBtn.setEnabled(false);
            settingsProfileSaveBtn.setTextColor(getResources().getColor(R.color.disabledButton));
        }
    }

    // VISUAL EFFECTS
    private void applyConstraintsInstructions(ConstraintSet openedConstraintSet) {
//        ConstraintLayout constraintLayout = binding.settingProfileCard;
//        ConstraintSet constraintSet = new ConstraintSet();
//
//
//
//        constraintSet.clear(binding.settingsProfilePic.getId(), ConstraintSet.END);
//        constraintSet.connect(binding.settingsProfilePic.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID,ConstraintSet.END);
////                constraintSet.connect(R.id.imageView,ConstraintSet.TOP,R.id.check_answer1,ConstraintSet.TOP,0);
//        constraintSet.applyTo(binding.settingProfileCard);
        openedConstraintSet.connect(profilePicHolder.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END);
        openedConstraintSet.connect(usernameHolder.getId(), ConstraintSet.TOP, profilePicHolder.getId(), ConstraintSet.BOTTOM, 25);
        openedConstraintSet.connect(usernameHolder.getId(), ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START);
        openedConstraintSet.setHorizontalBias(usernameHolder.getId(), 0.4f);
        openedConstraintSet.connect(settingsProfileCancelBtn.getId(), ConstraintSet.TOP, usernameHolder.getId(), ConstraintSet.BOTTOM, 105);
        openedConstraintSet.connect(settingsProfileSaveBtn.getId(), ConstraintSet.TOP, usernameHolder.getId(), ConstraintSet.BOTTOM, 105);
    }

    private void closeCardView() {

        usernameHolder.setFocusable(false);

        settingsProfileCancelBtn.setVisibility(View.GONE);
        settingsProfileSaveBtn.setVisibility(View.GONE);

        addNewImageBtn.animate().scaleX(1.3f).scaleY(1.3f).setDuration(200).withEndAction(new Runnable() {
            @Override
            public void run() {
                addNewImageBtn.animate().scaleX(0.0f).scaleY(0.0f).setDuration(200)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {

//                                profilePicHolder.animate().scaleX(1f).scaleY(1f).setDuration(200).withEndAction(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                    }
//                                });
                            }});
            }
        });

        closedConstraintSet.applyTo(constraintLayout);
        binding.settingProfileCard.setEnabled(true);
    }

    private void openCardView() {
        openedConstraintSet.applyTo(constraintLayout);

        addNewImageBtn.animate().scaleX(1.3f).scaleY(1.3f).setDuration(200).withEndAction(new Runnable() {
            @Override
            public void run() {
                addNewImageBtn.animate().scaleX(1.0f).scaleY(1.0f).setDuration(200);
            }
        });


        profilePicHolder.animate().scaleX(2f).scaleY(2f).setDuration(200).withEndAction(new Runnable() {
            @Override
            public void run() {
                addNewImageBtn.animate().scaleX(1.0f).scaleY(1.0f).setDuration(200);
            }
        });


        settingsProfileCancelBtn.setVisibility(View.VISIBLE);
        settingsProfileSaveBtn.setVisibility(View.VISIBLE);

        usernameHolder.setFocusable(true);
        usernameHolder.setFocusableInTouchMode(true);
        usernameHolder.setClickable(true);

        binding.settingProfileCard.setEnabled(false);
    }
}