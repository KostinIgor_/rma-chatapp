package com.example.chatapp.util.constant;

public enum MSG_VIEW_TYPE {
    SENDER_VIEW_TYPE(1),
    RECEIVER_VIEW_TYPE(2);

    private final int id;

    MSG_VIEW_TYPE(final int id) {
        this.id = id;
    }

    public int getId() { return id; }
}
