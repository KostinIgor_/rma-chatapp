package com.example.chatapp.util.msgHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public abstract class  MessageViewHolderAbs extends RecyclerView.ViewHolder {
    protected TextView message, time;

    public MessageViewHolderAbs(@NonNull View itemView) {
        super(itemView);
    }

    public TextView getMessage() {
        return message;
    }

    public void setMessage(TextView message) {
        this.message = message;
    }
    public void setMessageText(String message) {
        this.message.setText(message);
    }

    public TextView getTime() {
        return time;
    }

    public void setTime(TextView time) {
        this.time = time;
    }

    public void setTimeText(String time) {
        this.time.setText(time);
    }
}