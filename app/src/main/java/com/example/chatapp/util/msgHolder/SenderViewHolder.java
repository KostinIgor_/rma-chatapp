package com.example.chatapp.util.msgHolder;

import android.view.View;

import androidx.annotation.NonNull;

import com.example.chatapp.R;

public class SenderViewHolder extends MessageViewHolderAbs {
    public SenderViewHolder(@NonNull View itemView) {
        super(itemView);
        this.message = itemView.findViewById(R.id.senderText);
        this.time = itemView.findViewById(R.id.senderTime);
    }
}