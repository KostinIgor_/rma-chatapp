package com.example.chatapp.util.msgHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.chatapp.R;

public class ReceiverViewHolder extends MessageViewHolderAbs{
    public ReceiverViewHolder(@NonNull View itemView) {
        super(itemView);

        this.message = (TextView)itemView.findViewById(R.id.reciverText);
        this.time = itemView.findViewById(R.id.reciverTime);

    }
}
