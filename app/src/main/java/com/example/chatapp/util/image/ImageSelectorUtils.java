package com.example.chatapp.util.image;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import com.example.chatapp.R;
import com.squareup.picasso.Picasso;

public class ImageSelectorUtils {
    public static void setImage(String path, ImageView holder ){
        if (path == null || path.equals("")) {
            holder.setImageResource(R.drawable.avatar);
        } else if( path.charAt(0) == '/') {
            try{
                Bitmap bitmap = BitmapFactory.decodeFile(path);
                holder.setImageBitmap(bitmap);
                int check=bitmap.getWidth();
            }catch (Exception w){
                holder.setBackgroundResource(R.drawable.avatar);
            }
        }
        else {
            Picasso.get()
                    .load(path)
                    .placeholder(R.drawable.avatar)
                    .into(holder);
        }
    }

}
