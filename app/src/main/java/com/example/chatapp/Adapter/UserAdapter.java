package com.example.chatapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatapp.ChatDetailsActivity;
import com.example.chatapp.Models.MessageModel;
import com.example.chatapp.Models.User;
import com.example.chatapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder>{

    ArrayList<User> list; // List of users, initilized in constructor
    HashMap<String, Date> lastMessageMap = new HashMap<String, Date>();
    HashMap<String, User> usersList = new HashMap<String, User>();
    UserAdapter thisAdapter;

    Context context;

    public UserAdapter(ArrayList<User> list, Context context) {
        this.list = list;
        for (User p: list) { usersList.put(p.getUserId(), p); }
        this.context = context;
        this.thisAdapter = this;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_card_chats, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user = list.get(position); // getting user one by one

        Picasso.get()       // setting its profile picture
                .load(user.getProfilePic())   // loading users profile picture
                .placeholder(R.drawable.avatar3)  // using placeholder in case there was error wile loading picture
                .into(holder.image);  // storing picture in holder

        holder.userName.setText(user.getUserName());  // setting user's username in holder

        FirebaseDatabase.getInstance().getReference().child("Chats")
                .child(FirebaseAuth.getInstance().getUid() + user.getUserId())
                .orderByChild("timestamp")
                .limitToLast(1)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.hasChildren()){ // checking if there are any children(whether the users have any messages exchanged at all)
                            for(DataSnapshot msg : snapshot.getChildren()){ // iterating over children, although we know that there are only one child(message) because of filter limit, but still it is array
                                MessageModel msgM = msg.getValue(MessageModel.class);

                                holder.lastMessage.setText(msgM.getMessage()); // msg is whole message object, so we have to access its "message" property, and to convert to string

                                Date date = new Date(msgM.getTimestamp());
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E:HH:mm");
                                String strDate = simpleDateFormat.format(date);
                                holder.timeHolder.setText(strDate);
//                                lastMessageMap.put(users.getUserId(), date);
//
//                                LinkedHashMap<String, Date> sortedMap = new LinkedHashMap<>();
//                                ArrayList<Date> list = new ArrayList<>();
//
//                                for (Map.Entry<String, Date> entry : lastMessageMap.entrySet()) {
//                                    list.add(entry.getValue());
//                                }
//                                Collections.sort(list, new Comparator<Date>() {
//                                    public int compare(Date str, Date str1) {
//                                        return (str).compareTo(str1);
//                                    }
//                                });
//
//                                Collections.reverse(list);
//
//                                for (Date str : list) {
//                                    for (Map.Entry<String, Date> entry : lastMessageMap.entrySet()) {
//                                        if (entry.getValue().equals(str)) {
//                                            sortedMap.put(entry.getKey(), str);
//                                        }
//                                    }
//                                }
//                                lastMessageMap = sortedMap;
                            }
                        }
//                        lastMessageMap.put(users.getUserId(), Date.MI);
//                        System.out.println(lastMessageMap.toString());

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
//                .addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot snapshot) {
//                        if(snapshot.hasChildren()){ // checking if there are any children(whether the users have any messages exchanged at all)
//                            for(DataSnapshot msg : snapshot.getChildren()){ // iterating over children, although we know that there are only one child(message) because of filter limit, but still it is array
//                                holder.lastMessage.setText(msg.child("message").getValue().toString()); // msg is whole message object, so we have to access its "message" property, and to convert to string
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError error) {
//
//                    }
//                });

        holder.itemView.setOnClickListener(new View.OnClickListener() {  // setting on click listener so when logged user click on this user, there's chat will be displayed
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChatDetailsActivity.class); // preparing new intent to be opened
                intent.putExtra("userId", user.getUserId());  // Prosledjivanje podataka u otvoreni aktiviti
                intent.putExtra("profilePic", user.getProfilePic());
                intent.putExtra("username", user.getUserName());
                context.startActivity(intent); // otvarenje zahtevanog aktivitja
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        TextView userName,lastMessage, timeHolder;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.profilePic);
            userName = (TextView) itemView.findViewById(R.id.userNameList);
            lastMessage = (TextView) itemView.findViewById(R.id.lastMessage);
            timeHolder = (TextView) itemView.findViewById(R.id.timeHolder);
        }
    }
}
