package com.example.chatapp.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatapp.Models.MessageModel;
import com.example.chatapp.R;
import com.example.chatapp.util.msgHolder.MessageViewHolderAbs;
import com.example.chatapp.util.msgHolder.ReceiverViewHolder;
import com.example.chatapp.util.msgHolder.SenderViewHolder;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ChatAdapter extends RecyclerView.Adapter {

    RecyclerView view;
    Context context;
    ArrayList<MessageModel> messageModels;
    String reciverId;

    // TODO: CLEAN-UP -  enumeracija izdvojene u posebnu klasu
    int SENDER_VIEW_TYPE = 1;
    int RECIVER_VIEW_TYPE = 2;

    public ChatAdapter(ArrayList<MessageModel> messageModels, Context context) {
        this.context = context;
        this.messageModels = messageModels;
    }

    public ChatAdapter(ArrayList<MessageModel> messageModels, Context context, String receiverId) {
        this.context = context;
        this.messageModels = messageModels;
        this.reciverId = receiverId;
    }

    public ChatAdapter(ArrayList<MessageModel> messageModels, Context context, String receiverId, RecyclerView view) {
        this.context = context;
        this.messageModels = messageModels;
        this.reciverId = receiverId;
        this.view = view;
    }

    // TODO: CLEAN-UP
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == SENDER_VIEW_TYPE) {
            return new SenderViewHolder(LayoutInflater.from(context).inflate(R.layout.sender_msg_bubble, parent, false));
        } else {
            return new ReceiverViewHolder(LayoutInflater.from(context).inflate(R.layout.receiver_msg_bubble, parent, false));
        }
    }

    // TODO: CLEAN-UP
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MessageModel messageModel = messageModels.get(position);

        Date date = new Date(messageModel.getTimestamp());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        String strDate = simpleDateFormat.format(date);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        String senderRoom = FirebaseAuth.getInstance().getUid() + reciverId;
        String reciverRoom = reciverId + FirebaseAuth.getInstance().getUid();

        ((MessageViewHolderAbs) holder).setMessageText(messageModel.getMessage());
        ((MessageViewHolderAbs) holder).setTimeText(strDate);

        if(view != null){
            view.scrollToPosition(position);
        }

        if (holder.getClass() == SenderViewHolder.class) {
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    new AlertDialog.Builder(context)
                            .setTitle("Delete")
                            .setMessage("Are you sure you want to delete this message?")
                            .setPositiveButton("Delete for me", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    database.getReference().child("Chats")
                                            .child(senderRoom)
                                            .child(messageModel.getMessageId())
                                            .setValue(new MessageModel(messageModel.getUserId(), "Deleted message", messageModel.getTimestamp()));

                                }
                            })
                            .setNegativeButton("Delete for everyone", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    database.getReference().child("Chats")
                                            .child(senderRoom)
                                            .child(messageModel.getMessageId())
                                            .setValue(new MessageModel(messageModel.getUserId(), "Deleted message", messageModel.getTimestamp()))
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void unused) {
                                                    database.getReference().child("Chats")
                                                            .child(reciverRoom)
                                                            .child(messageModel.getMessageId())
                                                            .setValue(new MessageModel(messageModel.getUserId(), "Deleted message", messageModel.getTimestamp()))
                                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                @Override
                                                                public void onSuccess(Void unused) {

                                                                }
                                                            });
                                                }
                                            });
                                }
                            })
                            .setNeutralButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .show();

                    return false;
                }
            });
        } else if (holder.getClass() == ReceiverViewHolder.class) {
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    new AlertDialog.Builder(context)
                            .setTitle("Delete")
                            .setMessage("Are you sure you want to delete this message?")
                            .setPositiveButton("Delete for me", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                                    String senderRoom = FirebaseAuth.getInstance().getUid() + reciverId;

                                    database.getReference().child("Chats")
                                            .child(senderRoom)
                                            .child(messageModel.getMessageId())
                                            .setValue(new MessageModel(messageModel.getUserId(), "Deleted message", messageModel.getTimestamp()));

                                }
                            })
                            .setNeutralButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .show();

                    return false;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return messageModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (messageModels.get(position).getUserId().equals(FirebaseAuth.getInstance().getUid())) ? SENDER_VIEW_TYPE : RECIVER_VIEW_TYPE;
    }
}
