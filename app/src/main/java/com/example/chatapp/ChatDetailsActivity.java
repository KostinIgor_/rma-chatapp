package com.example.chatapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.chatapp.Adapter.ChatAdapter;
import com.example.chatapp.Models.MessageModel;
import com.example.chatapp.databinding.ActivityChatDetailsBinding;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;

public class ChatDetailsActivity extends AppCompatActivity {

    ActivityChatDetailsBinding binding; // klasa koja referncirea na ActivityChatDetails layout
    ImageView profilePicHolder;
    TextView usernameHolder;

    FirebaseAuth mAuth;
    FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_details);

        binding = ActivityChatDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        getSupportActionBar().hide(); // sakrivanje gorenjeg bar

        database = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();

        final String idSender = mAuth.getUid(); //  Dobavaljanje id ulogovanog korisnika
        final String idReceiver = getIntent().getStringExtra("userId");  // ekstraktovanje id koirniska ciji je chat otvoreren
        String userName = getIntent().getStringExtra("username");
        String image = getIntent().getStringExtra("profilePic");

        profilePicHolder = binding.profilePic;
        usernameHolder = binding.userName;

        usernameHolder.setText(userName);
        Picasso.get()
                .load(image)
                .placeholder(R.drawable.avatar)
                .into(profilePicHolder);

        binding.backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChatDetailsActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        final ArrayList<MessageModel> messageModels = new ArrayList<MessageModel>();
        final ChatAdapter chatAdapter = new ChatAdapter(messageModels, this, idReceiver);

        binding.chatRecyclerView.setAdapter(chatAdapter); // postavljamo adaper za chat u recyclerView sa id chatRecyclerView, tako da tu moze da se renderjue chat pomocu adaptera

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        binding.chatRecyclerView.setLayoutManager(layoutManager); // elemetni u rcyView su rasporedjeni na osnocu menagera, koji mu se mora proslediti

        final String senderRoom = idSender + idReceiver;
        final String receiverRoom = idReceiver + idSender;

        database.getReference().child("Chats")
                .child(senderRoom)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        messageModels.clear();

                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {

                            MessageModel messageModel = dataSnapshot.getValue(MessageModel.class);
                            assert messageModel != null;
                            messageModel.setMessageId(dataSnapshot.getKey());
                            messageModels.add(messageModel);
                        }

                        chatAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

        binding.sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = binding.enterMessage.getText().toString(); // extracting message text form input field

                final MessageModel model = new MessageModel(idSender, message); // creating message object with adequate data
                model.setTimestamp(new Date().getTime()); // setting date of message to current date

                binding.enterMessage.setText(""); // clean the edit text
                String messageId = database.getReference().child("Chats") // Creating a new (empty) msg in the database, and just retrieving it's uid.
                        .child(senderRoom)                                // Exactly that one uid is required so in both rooms(SenderRoom, ReceiverRoom) have same msg with same uid
                        .push()                                           // otherwise we would have two exactly the same msg in two different rooms but with different uid
                        .getKey();
                assert messageId != null;
                database.getReference().child("Chats") // Now we save(for real) msg in sender's Room under just retrieved uid
                        .child(senderRoom)
                        .child(messageId)
                        .setValue(model).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        database.getReference().child("Chats")  // We do the same, just now for receiver's Room
                                .child(receiverRoom)
                                .child(messageId)
                                .setValue(model).addOnSuccessListener(new OnSuccessListener<Void>(){
                            @Override
                            public void onSuccess(Void unused) {
                                binding.chatRecyclerView.scrollToPosition(messageModels.size()-1);
                            }
                        });
                    }
                });
            }
        });
    }
}