package com.example.chatapp.Models;

public class SuperUser extends User {
    MessageModel lastMessageM;

    public SuperUser(User user, MessageModel lastMessage) {
        this.userId = user.getUserId();
        this.profilePic = user.getProfilePic();
        this.userName = user.getUserName();
        this.mail = user.getMail();
        this.password = user.getPassword();
        this.lastMessage = user.getLastMessage();
        this.status = user.getStatus();

        this.lastMessageM = lastMessage;
    }

    public MessageModel getLastMessageM() {
        return lastMessageM;
    }

    public void setLastMessageM(MessageModel lastMessage) {
        this.lastMessageM = lastMessageM;
    }
}
